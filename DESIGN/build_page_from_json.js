function build_page_from_json(json){

  var fs = require('fs');
  var files = fs.readdirSync('/js/gens/CORE/');
  alert("test");
  alert(JSON.stringify(files));
  alert("test2");

  ready(function(){
    var this_page = document.getElementById('begin_json_parsing');
    if (this_page == null){
      this_page = document.getElementsByTagName('body')[0];
    }
    raw_contents = JSON.parse(this_page.innerHTML);
    this_page.innerHTML = "";
    this_page.appendChild(parse_it(this_page, this_page.parent, raw_contents));

  });

  // Include sub-generators js files
  (function(document, tag) {
      var scriptTag = document.createElement(tag), // create a script tag
          firstScriptTag = document.getElementsByTagName(tag)[0]; // find the first script tag in the document
      scriptTag.src = 'your-script.js'; // set the source of the script to your script
      firstScriptTag.parentNode.insertBefore(scriptTag, firstScriptTag); // append the script to the DOM
  }(document, 'script'));

  function ready(fn) {
    if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  }

  function parse_it(object, parent = false, data = {}, arguements = {}){
    // alert("OBJECT:" + JSON.stringify(object)); alert("DATA:" + JSON.stringify(data));
    console.log(JSON.stringify(data));
    var element_type = "div";
    // static table now, should later be populated from core and mods folders
    var allowed_array = ['HTML', 'LAST_HTML', 'PAGE', 'GENERATOR', 'POSTFORM', 'SELECTLIST', 'TABLE', 'ROW', 'CELL', 'FORM', 'SELECT', 'ROW_ADDER', 'COUNT', 'PERCENTAGE_MULTI_RANGE_SLIDER', 'UPDATING_CHECKBOX_LIST'];
    if (data.hasOwnProperty("arguments") && allowed_array.indexOf(data.arguments.process_type) > -1){
      if(data.arguments.hasOwnProperty("html_tag")){
        element_type = data.arguments.html_tag
      }
      if (arguments.hasOwnProperty("elemental_override")){
        element_type = arguments.elemental_override;
      }
      var element = document.createElement(element_type);
      let function_name = "parse_" + data.arguments.process_type;
      if (parent != false && parent != null){
        parent.appendChild(executeFunctionByName(function_name, window, element, parent, data.data, data.arguments));
      } else {
        element = executeFunctionByName(function_name, window, element, parent, data.data, data.arguments);
      }
      return element;
    } else {
      var element = document.createElement(element_type);
      if (arguments.hasOwnProperty("elemental_override")){
        element_type = arguments.elemental_override;
      }
      element.innerHTML = object;
      parent.appendChild(element);
      return element;
    }
  }

  function ajax(url, method, data, async)
  {
      method = typeof method !== 'undefined' ? method : 'GET';
      async = typeof async !== 'undefined' ? async : false;

      if (window.XMLHttpRequest)
      {
          var xhReq = new XMLHttpRequest();
      }
      else
      {
          var xhReq = new ActiveXObject("Microsoft.XMLHTTP");
      }


      if (method == 'POST')
      {
          xhReq.open(method, url, async);
          xhReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xhReq.setRequestHeader("X-Requested-With", "XMLHttpRequest");
          xhReq.send(data);
      }
      else
      {
          if(typeof data !== 'undefined' && data !== null)
          {
              url = url+'?'+data;
          }
          xhReq.open(method, url, async);
          xhReq.setRequestHeader("X-Requested-With", "XMLHttpRequest");
          xhReq.send(null);
      }
      //var serverResponse = xhReq.responseText;
      //alert(serverResponse);
  }

  function open_json_page(string){
      return JSON.parse(string);
  }

  function parse_HTML(object, parent, data, data_arguments = {}){
    let my_element = document.createElement(object.HTMLType);
    if (object.hasOwnProperty("class_list")){
      my_element.classList.add(object.class_list);
    } else {
      my_element.classList.add(object.HTMLType);
    }
    if (parent){
      parent.appendChild(my_element);
    } else {
    document.body.appendChild(my_element);
    }
    if (object.HTMLType == "ul" || object.HTMLType == "ol"){
      if (!object.innervalue.hasOwnProperty("HTMLType")){
        object.innervalue.HTMLType = "li";
      }
    }
    my_element.innerHTML = parse_it(object.innervalue, object);
  }

  function parse_PAGE(object, parent, data, data_arguments = {}){
    let head = document.createElement("head");
    let body = document.createElement("body");
    document.appendChild(head);
    document.appendChild(body);
    if (object.hasOwnProperty("head_area")){
      parse_it(object.head_area, object);
    }
    if (object.hasOwnProperty("body_area")){
      parse_it(object.body_area, object);
    }
  }

  function parse_GENERATOR(object, parent, data, data_arguments = {}){
    let generator = object.generatorPage;
    let default_type = object.HTMLType;
    // ajax, grab values
    let parse_this = ajax(generator, "GET", "");
    parse_this.HTMLType = default_type;
    parse_it(parse_this, object);
  }

  function parse_POSTFORM(object, parent, data, data_arguments = {}){
    let form = document.createElement("form");
    parent.appendChild(form);
    if (object.hasOwnProperty("inputElemenmts")){
      parse_it(object.inputElements, form);
    }
  }

  function parse_SELECTLIST(object, parent, data, data_arguments = {}){
    if (object.hasOwnProperty("listed_here")){
      if (object.listed_here == true){

      } else {

      }
    }

  }

  function parse_TABLE(object, parent, data, data_arguments = {}){
    /*
    var data = "";

    if (object.hasOwnProperty("listed_here")){
      if (object.listed_here == true){
        data = object.table_data;
      } else {
        // Fetch JSON data from URL
      }
    } else {
      data = object;
    } */
    var the_table = document.createElement("table");
    // formatting each row
    console.log("1");
    if (data_arguments.hasOwnProperty("column_header_type")){
      console.log("2");
      if (data_arguments.column_header_type == "cell_headers"){
        console.log("3");
          var reversed_data = swap(data[0]);
          var reversed_object = swap(the_table[0]);
          console.log(JSON.stringify(reversed_data));
          console.log(JSON.stringify(reversed_object));
          the_table.appendChild(parse_ROW(reversed_object, the_table, reversed_data));
      }
    }


    for (let i=0, len = count(data); i < len; i++){
      the_table.appendChild(parse_ROW(the_table[i], the_table, data[i]));
    }
    if (parent != null && parent != false){
        parent.appendChild(the_table);
    }
    return the_table;
  }

  function parse_ROW(object, parent, data, data_arguments = {}){
    var the_row = document.createElement("tr");
    console.log(count(data) + ":" + JSON.stringify(data));
    var keys = Object.keys(data);

    for(var i = 0; i < keys.length;i++){
       //keys[i] for key
       //dictionary[keys[i]] for the value
    }
    for (let i=0, len=keys.length; i<len; i++){
      the_row.appendChild(parse_CELL(the_row[keys[i]], the_row, data[keys[i]]));
    }
    if (parent != null && parent != false){
        parent.appendChild(the_row);
    }
    return the_row;
  }

  function parse_CELL(object, parent, data, data_arguments = {}){
    // alert("cello"+JSON.stringify(object) + "cellp"+JSON.stringify(parent) + "celld"+JSON.stringify(data));
    var the_cell = document.createElement("td");
    the_cell.appendChild(parse_it(data, the_cell, data, data_arguments));
    return the_cell;
  }

  function parse_LAST_HTML(object, parent, data, data_arguments = {}){
    let my_element = document.createElement(object.HTMLType);
    if (object.hasOwnProperty("class_list")){
      my_element.classList.add(object.class_list);
    } else {
      my_element.classList.add(object.HTMLType);
    }
    if (parent){
      parent.appendChild(my_element);
    } else {
    document.body.appendChild(my_element);
    }
    my_element.innerHTML = object.innerValue;
  }

  function parse_FORM(object, parent, data, data_arguments = {}){
    var the_form = document.createElement("form");
    // formatting each row
    console.log("1");
    if (data_arguments.hasOwnProperty("")){
      console.log("2");
    }
    for (let i=0, len = count(data); i < len; i++){
      alert(JSON.stringify(data_arguments["sub_arguements"][i]));
      the_form.appendChild(parse_it(the_form[i], the_form, data[i], data_arguments["sub_arguements"][i]));
    }
    if (parent != null && parent != false){
        parent.appendChild(the_form);
    }
    return the_form;
  }

  function executeFunctionByName(functionName, context /*, args */) {
      var args = Array.prototype.slice.call(arguments, 2);
      var namespaces = functionName.split(".");
      var func = namespaces.pop();
      for (var i = 0; i < namespaces.length; i++) {
          context = context[namespaces[i]];
      }
      return context[func].apply(context, args);
  }

  function element_conversion(element, target_type){
    var element = document.createElement('div');
    var newEl = document.createElement('form');
    if (element.hasAttributes()) {
        var attr = element.attributes
        for (var i = 0; i < attr.length; i++) {
            var name = attr[i].name, val = attr[i].value;
            newEl.setAttribute(name, val)
        }
    }

    if (element.innerHTML) { newEl.innerHTML = element.innerHTML }

    // document.body.appendChild(newEl);
    element = newEl;
    // Tip: There are certain properties you need to account for like innerHTML, innerText, and textContent that would overwrite one another if multiples are set. You may also have to account for childNodes and what not.
    return element;
  }

  function swap(json){
    var ret = {};
    for(var key in json){
      ret[json[key]] = key;
    }
    return ret;
  }

  function count(obj) {
    if (typeof obj === "undefined") {
        return 0;
    }
    return Object.keys(obj).length;
  }

}

function include_blocktype_javascript(url){

}
function
