# DESIGN Layer

## Ideal DESIGN layer

- Functions focused on client-side scripting.
- Should function purely by modifying DEPLOYMENT-provided documents and applying
  client-side quality-of-life improvements.
- Any URLs, form submissions, AJAX requests, etc. used should be supplied by the
  deployment layer.
- Sends data to the DATA layer only using forms and methods built into the
  DEPLOYMENT layer.
- Should be able to be turned off by the user to allow for standard HTML navigation.
- Should be agnostic to design of the DEPLOYMENT layer structure. Pages should
  dynamically adapt to provided DEPLOYMENT layer, applying to the whole site
  regardless if the entire DEPLOYMENT layer is restructured. (e.g. A "services"
  DEPLOYMENT page should *not* have a corresponding "services" javascript. A
  "services" page should have divs and tables that load, and there should be
  global javascript interpreters that are loaded recognize various types of
  tables and format their navigation options appropriately that would also load
  and function on a "products" page just as well.)

## Explanation of the DESIGN layer
The purpose of the DESIGN layer is to dynamically allow navigation of the site in it's
prefered designed way. It should be used for quality-of-life improvements of
site navigation and interaction without being neccesarry for any particular
site function.

Examples may include:
- Auto refreshing a page after submitting a form.
- Fetching changing data from a url that is supplied by DEPLOYMENT
- Refactoring a paging list into an infinite scroll
- Creating navigational buttons that while not adding new features, simplify
  use of existing features
- Turn what could be dozens or hundreds of pages of static html pages into dynamically
  interacting javascript-controlled pages.
- Fetching updated notifications that would otherwise only be updated on page load.

## Plans
- [ ]

## Layout of the DESIGN layer
