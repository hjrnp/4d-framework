Design of a blocktype.js file

First, the file will have a name with the prefix "blocktype_" and will be a js file.
The file will contain a function that has the blocktype name, and this will be the launching function.
The function will return html code based on the passed in parameter object.
The html returned should be free of any styleing in any way that isn't inherent to
its purpose: for example: data contained in a hidden, or an image being managed. 
