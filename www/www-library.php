<?php

# We need to be able to check if an ip_address in a particular range
function ip_in_range($needle_ip_address, $lower_range_ip_address, $upper_range_ip_address)
{
    # Get the numeric reprisentation of the IP Address with IP2long
    $min    = ip2long($lower_range_ip_address);
    $max    = ip2long($upper_range_ip_address);
    $needle = ip2long($needle_ip_address);

    # Then it's as simple as checking whether the needle falls between the lower and upper ranges
    return (($needle >= $min) AND ($needle <= $max));
}
