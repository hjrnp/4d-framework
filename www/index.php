<?php
require_once("www-settings.php");
require_once("www-library.php");

// Check against whitelist if applicable
if ($GLOBALS["4D"]["whitelist"]["on"]){
  if (!in_array($_SERVER['HTTP_X_FORWARDED_FOR'], $GLOBALS["4D"]["whitelist"]["list"]) && !in_array($_SERVER['HTTP_CLIENT_IP'], $GLOBALS["4D"]["whitelist"]["list"]) && !in_array($_SERVER['REMOTE_ADDR'], $GLOBALS["4D"]["whitelist"]["list"])){
      die("not allowed ip");
  }
  foreach ($GLOBALS["4D"]["whitelist"]["range_list"] as $from_ip => $to_ip){
    if (!ip_in_range($_SERVER['HTTP_X_FORWARDED_FOR'], $from_ip, $to_ip)){
      die("not allowed ip");
    }
    if (!ip_in_range($_SERVER['HTTP_CLIENT_IP'], $from_ip, $to_ip)){
      die("not allowed ip");
    }
    if (!ip_in_range($_SERVER['REMOTE_ADDR'], $from_ip, $to_ip)){
      die("not allowed ip");
    }
  }
}


// Check against blacklist if applicable
if ($GLOBALS["4D"]["blacklist"]["on"]){
  if (in_array($_SERVER['HTTP_X_FORWARDED_FOR'], $GLOBALS["4D"]["blacklist"]["list"]) || in_array($_SERVER['HTTP_CLIENT_IP'], $GLOBALS["4D"]["blacklist"]["list"]) || in_array($_SERVER['REMOTE_ADDR'], $GLOBALS["4D"]["blacklist"]["list"])){
      die("banned ip");
  }
  foreach ($GLOBALS["4D"]["blacklist"]["range_list"] as $from_ip => $to_ip){
    if (!ip_in_range($_SERVER['HTTP_X_FORWARDED_FOR'], $from_ip, $to_ip)){
      die("not allowed ip");
    }
    if (!ip_in_range($_SERVER['HTTP_CLIENT_IP'], $from_ip, $to_ip)){
      die("not allowed ip");
    }
    if (!ip_in_range($_SERVER['REMOTE_ADDR'], $from_ip, $to_ip)){
      die("not allowed ip");
    }
  }
}


// getting formatting of our current url
if (isset($_SERVER['REQUEST_URI'])) {
    $parse = parse_url(
        (isset($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'off') ? 'https://' : 'http://') .
        (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : (isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '')) . (($full) ? $_SERVER['REQUEST_URI'] : null)
    );
    $parse['port'] = $_SERVER["SERVER_PORT"]; // Setup protocol for sure (80 is default)
    $GLOBALS["4D"]["4D_URL"]["current_raw_url"] = http_build_url('', $parse);
} else {
  die("invalid_reqeust");
}
unset($parse);
$GLOBALS["4D"]["4D_URL"]["cleaned_url"] = str_replace("..", "", $GLOBALS["4D"]["4D_URL"]["current_raw_url"]);
$GLOBALS["4D"]["4D_URL"]["parsed_url"] = parse_url($GLOBALS["4D"]["4D_URL"]["cleaned_url"]);
$GLOBALS["4D"]["4D_URL"]["parsed_url"]["path_array"] = explode("/", $GLOBALS["4D"]["4D_URL"]["parsed_url"]["path"]);
$GLOBALS["4D"]["4D_URL"]["TOP_DIR"]= "";
$GLOBALS["4D"]["4D_URL"]["TOP_DIR"]= "";
switch($GLOBALS["4D"]["4D_URL"]["parsed_url"]["path_array"][1]){
  case "DATA":
    $GLOBALS["4D"]["4D_URL"]["TOP_DIR"]= $GLOBALS["4D"]["DIRS"]["DATA_DIRECTORY"];
    break;
  case "DEPLOYMENT":
    $GLOBALS["4D"]["4D_URL"]["TOP_DIR"]= $GLOBALS["4D"]["DIRS"]["DEPLOYMENT_DIRECTORY"];
    break;
  case "DESIGN":
    $GLOBALS["4D"]["4D_URL"]["TOP_DIR"]= $GLOBALS["4D"]["DIRS"]["DESIGN_DIRECTORY"];
    break;
  case "DISPLAY":
    $GLOBALS["4D"]["4D_URL"]["TOP_DIR"]= $GLOBALS["4D"]["DIRS"]["DISPLAY_DIRECTORY"];
    break;
  default:
    die("invalid url");
}
$GLOBALS["4D"]["4D_URL"]["sub_url"] = implode("/", array_slice($GLOBALS["4D"]["4D_URL"]["parsed_url"]["path_array"], 2));

// check final directory is technically valid
$temp_realpath = realpath($GLOBALS["4D"]["4D_URL"]["TOP_DIR"] . "/www/" . $GLOBALS["4D"]["4D_URL"]["sub_url"]);
$temp_query = realpath($GLOBALS["4D"]["4D_URL"]["TOP_DIR"] . "/www/");
if (substr($temp_realpath, 0, strlen($temp_query)) === $temp_query){
  unset($temp_realpath);
  unset($temp_query);
} else {
  die("illegal path");
}

require_once($GLOBALS["4D"]["4D_URL"]["TOP_DIR"]. "/www/index.php");
