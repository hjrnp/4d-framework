# www directory

The www directory presented in this repo is more a demonstration than a
requirement or feature. Although usable, and it exists to make the 4d framework
more 'out of the box' friendly, it's merely one way to control
pathing with the 4D framework along with a few basic features. For larger
deployments, it'd likely be prudent to put all of the 4D directories on different
servers and build your own routing system for your particular needs.

## Server suggestions for migrating away from the demo www directory

### DATA server

The DATA server is likely to be your most data intensive for obvious reasons,
and likely should be built more around large database servers. Further,
these servers should likely avoid caching, since up-to-date data is more likely
to be important, and on fast retrieval and whatever intense data functions you
build it around. For direct RESTful accessing users who will likely do bulk
transfers, this should be a high bandwidth connection.

### DEPLOYMENT server

The DEPLOYMENT server will likely be focused on processing and building websites,
but will be the primary server seen by users and search engine spiders.
As such, it should likely be built lean and fast with minimal latancy. This means
multiple small distributed servers in many locations to allow easy access.
It would be smart to make use of server-side caching on these servers for
commonly built code pieces, assuming it monitors for updates to invalidate old
caches.

Although it will be fetching from the DATA server, that initial load is very
important.

### DESIGN server

The DESIGN server will likely be focused on loading JS files. It should be a
small and fairly static server, prepped for client-side cacheing.

### DISPLAY
The DISPLAY server will be focused on building CSS files and smilar for users.
It should be a small and fast server, also prepped for client-side caching.
