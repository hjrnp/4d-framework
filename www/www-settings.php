<?php

// Location of the 4D directories
$GLOBALS["4D"]["DIRS"]["DATA_DIRECTORY"] = (dirname(__FILE__, 2) . "/DATA/");
$GLOBALS["4D"]["DIRS"]["DEPLOYMENT_LOCATION"] = (dirname(__FILE__, 2) . "/DEPLOYMENT/");
$GLOBALS["4D"]["DIRS"]["DESIGN_LOCATION"] = (dirname(__FILE__, 2) . "/DESIGN/");
$GLOBALS["4D"]["DIRS"]["DISPLAY_LOCATION"] = (dirname(__FILE__, 2) . "/DISPLAY/");

// Any blacklisted IPs
$GLOBALS["4D"]["whitelist"] = ["on"=> false];
// $GLOBALS["4D"]["whitelist"]["individual_list"] = ["8.8.8.8", "1.1.1.1"];
// $GLOBALS["4D"]["whitelist"]["range_list"] = ["1.1.1.1" => "1.1.8.8", "8.1.1.1" => "8.1.8.8"];
$GLOBALS["4D"]["blacklist"] = ["on"=> false];
// $GLOBALS["4D"]["blacklist"]["individual_list"] = ["8.8.8.8", "1.1.1.1"];
// $GLOBALS["4D"]["whitelist"]["range_list"] = ["1.1.1.1" => "1.1.8.8", "8.1.1.1" => "8.1.8.8"];
