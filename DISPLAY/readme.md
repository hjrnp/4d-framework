# DISPLAY Layer

## Ideal DISPLAY layer

- Does not modify any user data or data interaction
- Does not fetch any information from any other layer other than media files for design purposes.
- Is easily changed between different fetched files based on user choice for maximum accessibility.
- Color and design styles should be seperate from spacing styles.
- Recommended languages: Dynamically generated CSS

## Explanation of the DISPLAY layer
The point of the DISPLAY layer is to ensure that the service is most pleasing to experience
by the most varied selection of individuals.

The DISPLAY layer is *not* meant to provide any useful data.
It is meant to provide only visual improvmeent for those navigating the site.

Although not part of this project, we suggest looking at the css files of
[CSS Zen Garden](http://www.csszengarden.com) for an example of the type of css files interactions that this
layer should generate.

## Layout of the DISPLAY layer

### The config folders
The config folders are are for setting variables neccesarry for building the CSS.
Examples of config files might include:
- Webfolders that contain images
- storage for user-preferences

### The branding folder
This contains organization-specific branding. If multiple organizations are hosted,
this can contain branding for many of them.

### www
This is the folder where the CSS is actually delivered.

### color_modes
This folder is for storing color-control CSS-building files.

### sizing_modes
This folder is for storing sizing-control CSS-building files.
