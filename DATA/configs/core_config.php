<?php
  // This determines where the DATA server redirects users to if they
  // go to the root instead of making a rest call.
  $GLOBALS["index_redirect"] = ["example.com" => "/core_modules/restful_doc/"];
  // Databases that may be quried. Databases are sorted by name, for calling different
  // Databases for different data sets. Each of these is a non-keyed array that lists
  // databases that may be queried. The first will be default, but it allows others
  // to be placed for a fallback. 
  $GLOBALS["database_array"] = ["default" => ["localhost", "192.168.0.1"]];
  // Sets which of the environment-specific config folders you will be calling from.
  $GLOBALS["environment"] == "default";
