<?php

function config_fetch_environment($fetchWhat = -1){
  if ($fetchwhat == -1){
    $dir_contents = getDirContents("environments" . DIRECTORY_SEPARATOR . $GLOBALS["environment"]. DIRECTORY_SEPARATOR);
    foreach ($dir_contents as $dir_content){
      require_once($dir_content);
    }
  } else {
    require_once("environments" . DIRECTORY_SEPARATOR . $GLOBALS["environment"]. DIRECTORY_SEPARATOR . $fetchWhat);
  }
}

function getDirContents($dir){
    $results = array();
    $files = scandir($dir);

        foreach($files as $key => $value){
            if(!is_dir($dir. DIRECTORY_SEPARATOR .$value)){
                $results[] = $value;
            } else if(is_dir($dir. DIRECTORY_SEPARATOR .$value)) {
                $results[] = $value;
                getDirContents($dir. DIRECTORY_SEPARATOR .$value);
            }
        }
}
