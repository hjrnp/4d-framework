<?php

$GLOBALS["core_modules"]["database"]["db_structure"] =
[
  "scopes" => [
    "scope_id"=>["type"=>"int", "PK" => true, "AutoIncrement" => true],
    "scope_name"=>["type"=>"string"]
  ],
  "scope_relations" => [
    "scope1_id" => ["type"=>"int"],
    "relation" => ["type"=>"enum"],
    "scope2_id" => ["type"=>"int"]
  ]
];
