<?php
require_once("databaseConfig.php");
require_once("permissions.php");

/*
if ($_GET["THERE_IS_NO_SPOON"]=="ewo49fzldf249850238fdsjlkfafas0"){
  // DB Test
  echo "<pre>";
//  print_r(get_tables());
//  print_r(get_columns("abilities"));
// print_r(simple_select_query(["id", "name", "is_specialization", "spec_parents", "spe", "fin", "mig", "tou", "iq", "cre", "mem", "san", "creator_id"], "abilities"));
// print_r(fetch_abilities());
print_r(simple_select_query(["id", "name", "point_cost", "categories", "prerequisites", "creator_categories"], "aspects")); // mysqli_query($link, $SQL2);

  echo "</pre>";
}
*/


function get_option_sets($where = "ALL", $global_db = $GLOBALS["db"]){
  switch ($where){
    case "ALL":
    default:
      $where_clause = "";
      break;
  }
    $query = <<< SQL
    SELECT  *
       FROM `option_sets`
       $where_clause;
SQL;
    $command = $global_db->prepare($query);
}

function run_command($command, $variables, $global_db = $GLOBALS["db"]){
  $prepped = $$global_db->prepare($command);
}

function cycle_results($sql, $global_db = $GLOBALS["db"]){
  // Need to fix to return as asssociative array?
  $return_array = [];
  foreach ($$global_db->query($sql) as $row){
    $return_array[] = $row;
  }
  return $return_array;
}

function cycle_prepared_results($sql, $variable_array, $global_db = $GLOBALS["db"]){
  $stmt = $global_db->prepare($sql);
  $count = 1;
  foreach ($variable_array as $value){
    $stmt->bindParam($count, $value);
    $count++;
  }
  $stmt->execute();
  $return_array = [];
  if ($stmt->execute()) {
    // use PDO::FETCH_ASSOC in fetch?
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $return_array[] = $row;
    }
  }
  return $return_array;
}

function cycle_zero_results($sql, $global_db = $GLOBALS["db"]){
  $return_array = [];
  foreach ($global_db->query($sql) as $row){
    $return_array[] = $row[0];
  }
  return $return_array;
}


function prep_reusult($result){
  echo json_encode($result, true);
}

// Note: Bulid table select does not verify
function build_table_select_list($columns){
  $column_list = "SELECT ";
  $after_first = false;
  foreach ($columns as $column){
    if ($after_first == true){
      $column_list .= ", ";
    } else {
      $after_first = true;
    }
    $column_list .= "`" . $column . "`";
  }
  // echo ($column_list);
  return $column_list;
}

// Note: Build Where does not verify
function build_table_where($where_array){
  ensure_array($where_array);
  if (!empty($where_array)){
    $first_where = true;
    $return_where = " WHERE ";
    foreach ($where_array as $column => $value){
      if ($first_where == false){
        $return_where .= ", ";
      } else {
        $return_where = false;
      }
      $return_where .= "`" . $column . "` = " . $value;
    }
  } else {
    return "";
  }
}

function build_table_insert_list($insert_array = [], $question_marks = false){
  $return_string = "";
  $table_string = "";
  $value_string = "";
  $first_cycle = true;
  foreach ($insert_array as $key => $value){
    if ($question_marks == true){
      $value_add = "?";
    } else {
      $value_add = $value;
    }
    if ($first_cycle == true){
      $table_string .= $key;
      $value_string .= $value_add;
    } else {
      $table_string .= ", " . $key;
      $value_string .= ", " . $value_add;
    }
  }

  if (count($insert_array) > 0){
    $return_string = "(" . $return_string . ") VALUES ";
  }
}

function simple_select_query($columns, $table, $where_is = []){
  ensure_array($where_is);
  $check_columns = $columns;
  if (!empty($where_is)){
    foreach ($where_is as $key => $value){
      $check_columns[] = $key;
    }
  }
  if (!verify_columns($check_columns, $table)){
    return false;
  }
  $sql = build_table_select_list($columns) . " FROM " . $table . build_table_where($where_is) . ";";
  $results = cycle_results($sql);
  return $results;
}

function simple_insert_query($table, $values = []){
  ensure_array($values);
  $check_columns = array_keys($values);
  if (!empty($values)){
    foreach ($values as $key => $value){
      $check_columns[] = $key;
    }
  }
  if (!verify_columns($check_columns, $table)){
    return false;
  }
  $sql = "INSERT INTO `" . $table . "` " . build_table_insert_list($values, true) . ";";
  $results = cycle_prepared_results($sql, $values);
  return $results;
}


// Note: Verify Columns also verifies table. No need to do both.
function verify_columns($columns_array, $table){
  ensure_array($columns_array);
  if (!verify_table($table)){
    return false;
  }
  $table_array = get_columns($table);
  foreach ($columns_array as $column){
    if (!in_array($column, $table_array)){
      return false;
    }
  }
  return true;
}

// Make sure the table exists
function verify_table($table){
  if (in_array($table, get_tables())){
    return true;
  } else {
    return false;
  }
}

// Get a list of columns in a table
function get_columns($table){
  if (verify_table($table)){
      $sql = "SHOW columns FROM `" . $table . "`;";
      // $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'deusdre1_raddb' AND TABLE_NAME = '$table';";
      return cycle_zero_results($sql);
  } else {
    return false;
  }
}


// Get a list of tables in the database
function get_tables(){
  $sql = "SHOW TABLES";
  $results = cycle_zero_results($sql);
  return $results;
}

function ensure_array(&$variable){
  if (!is_array($variable)){
    $variable = [$variable];
  }
}

function get_structure($table){
  if (verify_table($table)){
    $sql = "DESCRIBE `$table`;";
    $return_array = [];
    foreach ($GLOBALS["db"]->query($sql) as $row){
      $return_array[] = $row;
    }
    return $return_array;
  } else {
    return false;
  }
}
