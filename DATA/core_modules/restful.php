<?php
  require_once("permissions.php");
  require_once("databaseLink.php");
  $permission_array = ["userid" => $userid, "authkey" => $authkey, "method" => $_SERVER['REQUEST_METHOD'], "attempted_permissions"=> [], "request_target" => ["restLink" => $_SERVER['REQUEST_URI']]];
  $permissionType = check_access_permission($permission_array);
  if (!$permissionType){
    die("Permission Denied");
  }
