# DATA Layer

## Ideal DATA layer

- All site data should be accessible through the data layer.
- Should implement a standard interface (such as RESTful)
    - Data - we suggest GraphQL - this refers to readable data formats such as json, text, midi, etc.
    - Binaries - we suggest REST - this refers to binary data such as files, images, mp3s, etc. that GraphQL gives no advantage to.
- Recommended language: PHP
- Recommended Design Concepts: Stateless
- Return types: Static returns only
- Returned data should not include any html. JSON and static files (e.g. jpg) are preferred.

##Explanation of the DATA layer

The purpose of the DATA layer is to ensure that information is directely and
easily accessible regardless of the method of access as long as appropriate
access permissions exist.

It also is meant to ensure that the value the site gives is easily and readily
accessible for analysis and extension.  

## Layout of the DATA layer
There are 6 folders in the DATA layer root folder, and two files.

They are as follows:

readme.md <- this file, contains information on proper use of data layer.

www/ <- This is the only folder that should be visible from the web.

CORE_DO_NOT_EDIT/ <- These are parts of the 4D framework that are neccesarry. These
are files that are considered "global to all 4D". Changing anything here is liable
to break not only your files, but third party modules. Change or attempt to override
code here is considered high risk.

configs/ <- These are where configuration files go. They should ALL go here.
It creates a one-stop-shop for any IT folks that need to check configuration for this
layer. Any changable module configs, core configs, and your own configs should all
go here.

core_modules/ <- default modules, may be replaced by your own code or third
party modules if found to be better.

third_party_modules/ <- drop in third party modules here.

our_code/ <- the code you develop should go in this folder.
