# Order of events
The processing of any request goes through the following steps...

1. Intake
    - Intake of data
        - If data is encrypted, get user data
            - Use user data to decrypt information
        - If connection is requesting encryption, set up encryption
            - Establish encrypted connection with user
            - Store encrypted connection with duration
        - If data is not encrypted, check if access needs encrypted
            - If data does not need encryption, continue
            - If data needs encryption, check against honeypot count
                - If honeypot count not reached, increase honeypot count and try again
                - If honeypot count reached, flag source, present honeypot response
2. Fire start tasks
    - Lock user datastore altering activity to event
    - Fetch complete list of data needed from data store
    - Create datatore variable
3. Permission Checking
    - Check if user has permission to access target data
        - If user has permission, collect information
            - Reset Honeypot count
        - If user does not have permission, check if user has already requested against honeypot
            - If honeypot count not reached, increase honeypot count and request permission again
            - If honeypot count reached, temporarily lock user, present honeypot response
4. Processing Request
    - Process Request
5. Fire exit tasks
    - Verify legitimacy of datastore append action object
    - Append datastore changes to datastore todo action list
    - unlock user datastore activity
6. Return Request
    - If return is RESTful, build return using RESTful
    - If return is graphQL, build return using graphQL
    - If encrypted connection, encrypt data
    - Send Data

# Neccesarry DB/Datastore functionality
    - each row have a 'last modification' date.
    - each row has, at least, a unique ai
    - db must support ability to queue actions or store procedures
    - db must be able to lock user actions until their queue is completed if they perform a locking action
