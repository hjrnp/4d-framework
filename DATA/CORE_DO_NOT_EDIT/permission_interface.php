<?php
// This is a list of functions for basic permission calls.
// They are meant to be a reliable method of putting, updating, and fetching
// data regardless of the permission management system. They are a middle-man.
//
// third party modules and the code your organization creates
// should try to rely on these functions for permision checks, and
// third parties who made permission management modules should make sure they
// are compatible with this interface.

need_once("user_connector");
need_once("permission_interface");

function check_user_permissions(){
  return module_check_user_permissions();
}
