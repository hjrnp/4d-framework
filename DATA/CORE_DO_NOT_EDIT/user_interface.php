<?php
// This is a list of functions for basic user calls.
// They are meant to be a reliable method of putting, updating, and fetching
// data regardless of the user management system. They are a middle-man.
//
// third party modules and the code your organization creates
// should try to rely on these functions for database calls, and
// third parties who made user management connectivity modules should make sure they
// are compatible with this interface.

need_once("user_connector");
need_once("permission_interface");

function add_user($add_array){
  return module_add_user($add_array);
}
function del_user($del_array){
  return module_del_user($del_array);
}
function get_user_permissions($request_array){
  return module_get_user_permissions($request_array);
}
function modify_user_permissions($user_array, $permission_array){
  return module_modify_user_permissions($user_array, $permission_array);
}
function get_user_data($user_array){
  return module_get_user_data($user_array);
}
function modify_user_data($user_array, $modified_user_data_array){
  return module_modify_user_data($user_array, $modified_user_data_array);
}
function assign_user_access_token($user_array){
  return module_assign_user_access_token($user_array);
}
function validate_user_access_token($user_array){
  return module_validate_user_access_token($user_array);
}
