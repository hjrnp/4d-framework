<?php
// This is a list of functions for basic permission calls.
// They are meant to be a reliable method of putting, updating, and fetching
// data regardless of the permission management system. They are a middle-man.
//
// third party modules and the code your organization creates
// should try to rely on these functions for permision checks, and
// third parties who made permission management modules should make sure they
// are compatible with this interface.

need_once("output/router.php");
need_once("output/header_maker.php");
need_once("output/output_formatter.php");
need_once("output/final_processor.php");

function get_route($global_array = $GLOBALS){
  return module_get_route($global_array);
}

function designate_filetype($global_array = $GLBOALS){
  return module_designate_filetype($global_array);
}

function deliver_file($global_array = $GLOBALS){
  
}
