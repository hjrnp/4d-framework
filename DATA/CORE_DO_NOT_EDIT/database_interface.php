<?php
// This is a list of functions for basic database calls.
// They are meant to be a reliable method of putting, updating, and fetching
// data regardless of the database type. They are a middle-man.
//
// third party modules and the code your organization creates
// should try to rely on these functions for database calls, and
// third parties who made database connectivity modules should make sure they
// are compatible with this interface.

need_once("database_connector");
need_once("permission_interface");

// This function is for adding new entries into the db
function put($put_array=[]){
  return module_put($put_array);
}

// This function is for updating matching entries into the db
function update($update_array=[]){
  return module_update($update_array);
}

// This function is for removing matching entries into the db
function del($del_array=[]){
  return module_del($del_array);
}

// This function is for fetching matching entries from the db
function get($get_array=[]){
  return module_get($get_array);
}

// This function is to create table(s) in the db
function seed($seed_array=[]){
  return module_seed($seed_array);
}
