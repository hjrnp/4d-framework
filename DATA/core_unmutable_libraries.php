<?php

/* Need once will load a needed file/library. It will
 * load in order of priority:
 * CORE_DO_NOT_EDIT libraries, then "our code", then "third party modules", then "core modules"
 *
 */

function need_once($filename){
  if (folder_exists("CORE_DO_NOT_EDIT" . DIRECTORY_SEPARATOR . $filename)){
    require_once("CORE_DO_NOT_EDIT" . DIRECTORY_SEPARATOR . $filename);
  } elseif (folder_exists("our_code" . DIRECTORY_SEPARATOR . $filename)){
    require_once("our_code" . DIRECTORY_SEPARATOR . $filename);
  } elseif (folder_exists("third_party_modules" . DIRECTORY_SEPARATOR . $filename)){
    require_once("modules" . DIRECTORY_SEPARATOR . $filename);
  } elseif (folder_exists("core_modules" . DIRECTORY_SEPARATOR . $filename)){
    require_once("core_modules" . DIRECTORY_SEPARATOR . $filename);
  }
}

function folder_exists($dirname){
  $filename = dirname(__FILE__, 2) . $dirname . DIRECTORY_SEPARATOR;
  if (!file_exists($filename)) {
      return false;
      exit;
  } else {
      return true;
  }
}

function  load_modules($modules = []){
  
}
