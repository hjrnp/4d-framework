# DEPLOYMENT Layer

## Ideal DEPLOYMENT layer

- All site data should be presented as simple non-formatted Static HTML files
- Should not contain styling of any kind (it may set element IDs and classes for identification).
- Areas that would be preferred to be dynamic should instead have links to the content's current state.
- Should function by accessing the DATA layer's RESTful interface and building pages dynamically
- Should not use any javascript or client-side processing
- Should be navigable to reach anything on the website
- Should not build any HTML pages directly, but do so dynamically from data gained from the DATA layer.
- Is extremely easy for site indexers to navigate
- Ideally relies primarily on functional programming paradigms to preserve its stateless nature
- Primarily written in PHP or another server-side-only language.

## Explanation of the DEPLOYMENT layer
The point of the DEPLOYMENT layer is to ensure that the service is accessible on
nearly any end-user facing device.

The DEPLOYMENT is *not* meant to provide a pleasent and elegant interface.
It is meant to provide the most simplistic and bare-bones interface that supplies
all neccesarry data for interacting with the site.

Although not part of this project, we suggest looking at the index files of
[CSS Zen Garden](http://www.csszengarden.com) for an example of the type of html files that this
layer should generate.

## Layout of the DEPLOYMENT layer
