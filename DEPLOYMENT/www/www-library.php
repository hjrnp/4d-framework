<?php

function get_from_data($location_array, $the_globals = $GLOBALS){

}

function build_deployment($build){
  foreach ($build as $section => $section_data){
    if (is_numeric($section)){
      build_deployment($section_data);
    } else {
      echo "<" . $section;
      if (isset($section_data["parameters"])){
        foreach ($section_data["parameters"] as $parameter => $value){
          // enforce that the DEPLOYMENT layer has NO INHERENT STYLING!!
          if ($parameter == "style"){continue;}
          echo " $parameter=" . '"' . $value . '"';
        }
      }
      if (!isset($section_data["parameters"]["id"])){
        echo " id=" . '"' . hrtime(true) . '"';
      }
      if (isset($section_data["internal"])){
        echo ">";
        if (is_array($section_data["internal"])){
          build_deployment($section_data["internal"]);
        } else {
          echo $section_data["internal"];
        }
        echo "</" . $section . ">";
      } else {
        echo " />";
      }
    }
  }
}
