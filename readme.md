# 4D Framework
It is a sad tradition in web development that most sites are designed addressing a
few browsers, with little regard beyond that point. The 4D framework is designed
from the ground up to support the most possible clients while also simplifiying the
development and testing process and reducing points of complexity and increasing
ease of rapid development.


## Current Status
Pre-Alpha
## Origins
This project is an evolving and improving framework being built for the voteitall.org
project, but being designed to be easily accessible to other projects.
## Plans

- [ ] Finish Alpha DATA Layer
    - [ ] Order firing
- [ ] Finish Alpha DEPLOYMENT Layer
- [ ] Finish Alpha DESIGN Layer
- [ ] Finish Alpha DISPLAY Layer
- [ ] Default Architecture Functionalities
    - [ ] API versioning
    - [ ] Feature Flagging
    - [ ] Functional-Checker
    - [ ] Passwordless Login
    - [ ] Honeypot
    - [ ] Sessionless
    - [ ] DB Distributed Permissions
    - [ ] Server-Client End-to-End Encryption Even without network support (looking at you China and Republican US Senators!)
    - [ ] Switch out REST for a graphQL-compatible interface(?)

## Beyond MVC

The MVC framework was created in the 80's to simplify making code reusable, and was a paradigm
used regardless of if for app development or web development. This has led to
many attempts to shoehorn its ideals into a web setup that were less than ideal,
lead to fuzzy boundaries between the layers of MVC that were often confusing for
developers, and often seemed to create arbitrary seperation.

The 4D framework takes the goals and ideals of MVC, and moves them into a modern era for the
web by also combining it with common-sense seperation into web-accessible layers
that naturally break the code apart into its purposes and roles.

## Supporting Clients

4D is designed for sites to go beyond the standard practice of supporting 3 to 5
browsers to instead supporting all users, and doing so elegantly while also
reducing workload on developers, but folding multi-environment support into practices
that increase code reusability and testability.

This is acheived by breaking the website down into 4 distinct layers, the seperation
of which ensured that the website is easily accessible to all users.

The DATA layer contains all the sites pertinant data. As a RESTful interface, it
allows direct access to allowed data regardless of method of access. This means
the site data can easily be used in any system, program, automation, or interface
that supports this widely used webstandard. As the other layers *must* go through
the same interface for their presentation, it ensures that no data is left out.

Further, it provided a core of the site for easy testing for even the developers,
allowing for easy unit testing, for large organization users to expand child services off of, etc.

The DATA layer should be accessible to any allowed source with an internet connection.

The DEPLOYMENT layer ensures that even the most simple web browsers and user-tools
can navigate the site; be they old netscape browsers on a 20 year old computer,
little-known single-developer hobby web browsers, assorted wget file-fetch commands,
screen readers for the blind, the most modern and up to date browser, a text-only
commandline web browser, embedded browsers, or hastly assembled user-registration
booths at trade shows. Regardless if the site officially "supports" a browser,
all browsers should work easily with the DEPLOYMENT layer without difficulty. It
should always display in basic html without javascript or css.

The DESIGN layer is meant to provide ease of use and enjoyable navigation.
It is meant to simplify navigation. It should be written primarily in javascript,
and is meant be quality-of-life web navigation improvements.

The DISPLAY layer is meant to make the site pleasent, branded, unique, and
adaptable to various visual needs. Primarely written to deploy CSS, it should adapt
that CSS to user and organization preferences and needs, and be easily adaptable.
Code for styling spacing, styling coloring, and style localization should all be
seperate from eachother to maximized adaptability.

## Description


The 4d framework is meant as a replacement for the aging MVC framework that has been
around since the 1980's.

MVC was created focused on a central server with one or two standard interfaces,
and was more of a general-use architecture.

MVC is outdated, as sites migrate away from the idea of a single access method, to
the idea of an information hub that allows many different forms of interface, from
web-apps, to cell apps, to clients, to third-party data-mining, and more.

4D has the advantage of compartmentalizing different aspects of the creation
process, and being a web-centric architecture while being agnostic to the needs
of the backend.  

The 4D stands for "Data", "Deployment", "Display", and "Design", and reflects the
four ways that the host can be accessesed by users.


                 | <----------------------------------------------------->  DATA

                 | <--------------------------------------Deployment----------^

                 | <------------------------Display-----------^---------------^

                 | <------------Design---------^------------------------------^

"Data": Generally a restful interface. This is where database access happens,
and is made accessible to the outside world. Here, permission control is paramount.
Who can edit what is set here, and is method-agnostic. Either they have permission
to perform a task, or they do not. If they would be allowed to perform a task in
certain scenarios and not in other scenarios, the task should be broken into different
tasks based on permission. By abstracting this layer to a single interface, it
also becomes agnostic to what happens in the back end. Allowing for either a single
Database server with a simplistic accessibility layer, or a complex server farm with
machine learning systems and distributed access. It doesn't matter for the design,
as long as the data interface, such as a standard RESTful interface remains the same.
- Example Primary language: SQL with a web access control.
- This layer will be accessed by all users, either directly or indirectly.
- Slowdowns that happen primarely with this layer as their bottleneck can be generally identified as business
logic or easily mitigated with improved hardware.
- Primarily directly accessed by third-party business logic and your own other
layers.
- Interfaces to internal business logic.
- All of the site's information that can be accessible should be accessible through the data layer.
- This is the layer that permission handling happens at.
- Multiple instances of a data layer may exist, and will primarely be for the purpose
of regionalization.

"Deployment": The next layer. Although the Data layer is available to any user with
any kind of ability to navigate a RESTful interface, meaning anybody on the commandline
or with a plugin to a standard web browser that allows passing GET, FETCh, POST, etc. data,
Deployment sets a standard format for navigating that data. It should pull information
from the Database and present navigation of that Data, and what data will be available
to each step of navigation. It is important to note that this step should not include
any styling or code that runs on the user's machine. Data they receive from this layer should be
static, and created by internal servers.
- Example primary language: PHP
- This layer will be accessed by indirectly by all non-bulk users, many third-party apps,
and more. It is the middle-man for everything that you want to have a standard navigation.
- Slowdowns that happen here, but not at the Data layer can easily be identified as a
code problem, and are a sign to address technical debt or reconsider how much
data is being loaded on page navigation.
- Multiple deployment layers may exist, but will likely due so in a multi-service
setting, such as making a core RESTful site parent to many child sites. Such as
if the primary site was foo.com, child sites may include bar.foo.com, blog.food.com,
foo-reports.com, or someSocialMediaSite.com/affiliatedGroups/foo.

"Display": This is a bare-bones web interface. This layer should only have images
when they are of central purpose, such as a site logo or primary display image.
- Example primary language: Javascript
- This layer will be accessed by most app and web users. It will also be a the primary
 interface for users using screen readers. Javascript should render webpages as unstyled HTML,
 although identifier classes for the design layer to see are encouraged, design
 classes are not. - e.g. a class for designating tabular data is acceptable, but
 a class meant for flagging the object to be colored blue is not.
- General rule of thumb, navigating the site through this layer should be
"fully functional, but look ugly".
- Slowdowns that happen here are likely in javascript logic, and will vary in
speed from client and browser, or may be caused by excessive ajax or async calls
to the Deployment or Data layers.
- Display should be device agnostic.

"Design":
- Example primary language: CSS
- This layer is focused on device accessibility, branding, and layout controls.
- This layer should generally should be removable without breaking the site -
merely leaving it 'ugly'.
- This layer should be toggleable by users, both switching between different
design layers made accessible by host, and turning off altogether especially for
those with slow connections.


-----------

A few coding conventions:
- Any file named "foo_interface" is meant to be as a connection point for modules, your
  work is less likely to have you modify these files, and more likely to use them for reference
  or design to work with them.
- names should be generally complete words, and descriptive of the purpose of the variable.
  The one real exception is if an obvious name would cause a conflict, at which point a
  derivative is acceptable. This applies to both function names and and variable names.

## Origins
  This project is an evolving and improving framework being built for the voteitall.org
  project, but due to the nature of that project, is  being designed to be easily
  accessible to other child and parallel projects.

## Goals
  To be able to create a complete, beautiful, extensible, modifiable, dynamic,
  and globablly navigatable website from simply providing a RESTful inteface that
  also allows additional tweaking and features to be added later for unique
  features and interactions.
